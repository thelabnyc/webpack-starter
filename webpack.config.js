const webpack = require("webpack"); 
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const bourbon = require('node-bourbon').includePaths;
const SvgStore = require('webpack-svgstore-plugin');


// Loaders
var loaders = [
    {
        test: /\.js$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
        query: {
            presets: ['es2015']
        }
    },
    {
        test: /\.scss$/,
        loader: 'style!css!sass?includePaths[]=' + bourbon,
    },
    {
        test: /\.(jpg|gif|png|svg)(\?.*)?$/,
        loader: 'file',
    },
    {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader?name=/fonts/[name].[ext]',
    }
];


// Plugins
var plugins = [
    new SvgStore({
        svgoOptions: {
            plugins: [
                {
                    removeTitle: true,
                    removeStyleElement: true
                }
            ]
        },
        prefix: ''
    }),
    new HtmlWebpackPlugin({
        template: __dirname + "/index.html"
    })
];


var config = {

    entry: "./scripts.js",
    output: {
        path: path.resolve(__dirname, "build"),
        file: "scripts.js"
    },
    devServer: {
        hot: true,
        inline: true,
        compress: true,
        contentBase: './build',
        port: 8080
    },
    module: {
        loaders: loaders
    },
    plugins: plugins
};


module.exports = config;


